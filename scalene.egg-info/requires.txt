wheel>=0.36.1
rich>=10.7.0
cloudpickle>=2.2.1
Jinja2>=3.0.3
psutil>=5.9.2
numpy!=1.27,>=1.24.0
pydantic>=2.6

[:platform_system != "Darwin"]
nvidia-ml-py>=12.555.43

[:python_version < "3.9"]
astunparse>=1.6.3
